// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles

// eslint-disable-next-line no-global-assign
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  for (var i = 0; i < entry.length; i++) {
    newRequire(entry[i]);
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  return newRequire;
})({"../node_modules/htm/preact/standalone.js":[function(require,module,exports) {
var define;
!function(e,t){"object"==typeof exports&&"undefined"!=typeof module?t(exports):"function"==typeof define&&define.amd?define(["exports"],t):t(e.htmPreact={})}(this,function(e){var t=function(){},n={},o=[],r=[];function i(e,n){var i,l,a,s,p=arguments,u=r;for(s=arguments.length;s-- >2;)o.push(p[s]);for(n&&null!=n.children&&(o.length||o.push(n.children),delete n.children);o.length;)if((l=o.pop())&&void 0!==l.pop)for(s=l.length;s--;)o.push(l[s]);else"boolean"==typeof l&&(l=null),(a="function"!=typeof e)&&(null==l?l="":"number"==typeof l?l=String(l):"string"!=typeof l&&(a=!1)),a&&i?u[u.length-1]+=l:u===r?u=[l]:u.push(l),i=a;var c=new t;return c.nodeName=e,c.children=u,c.attributes=null==n?void 0:n,c.key=null==n?void 0:n.key,c}function l(e,t){for(var n in t)e[n]=t[n];return e}function a(e,t){null!=e&&("function"==typeof e?e(t):e.current=t)}var s="function"==typeof Promise?Promise.resolve().then.bind(Promise.resolve()):setTimeout,p=/acit|ex(?:s|g|n|p|$)|rph|ows|mnc|ntw|ine[ch]|zoo|^ord/i,u=[];function c(e){!e._dirty&&(e._dirty=!0)&&1==u.push(e)&&s(f)}function f(){for(var e;e=u.pop();)e._dirty&&L(e)}function d(e,t){return e.normalizedNodeName===t||e.nodeName.toLowerCase()===t.toLowerCase()}function h(e){var t=l({},e.attributes);t.children=e.children;var n=e.nodeName.defaultProps;if(void 0!==n)for(var o in n)void 0===t[o]&&(t[o]=n[o]);return t}function v(e){var t=e.parentNode;t&&t.removeChild(e)}function m(e,t,n,o,r){if("className"===t&&(t="class"),"key"===t);else if("ref"===t)a(n,null),a(o,e);else if("class"!==t||r)if("style"===t){if(o&&"string"!=typeof o&&"string"!=typeof n||(e.style.cssText=o||""),o&&"object"==typeof o){if("string"!=typeof n)for(var i in n)i in o||(e.style[i]="");for(var i in o)e.style[i]="number"==typeof o[i]&&!1===p.test(i)?o[i]+"px":o[i]}}else if("dangerouslySetInnerHTML"===t)o&&(e.innerHTML=o.__html||"");else if("o"==t[0]&&"n"==t[1]){var l=t!==(t=t.replace(/Capture$/,""));t=t.toLowerCase().substring(2),o?n||e.addEventListener(t,_,l):e.removeEventListener(t,_,l),(e._listeners||(e._listeners={}))[t]=o}else if("list"!==t&&"type"!==t&&!r&&t in e){try{e[t]=null==o?"":o}catch(e){}null!=o&&!1!==o||"spellcheck"==t||e.removeAttribute(t)}else{var s=r&&t!==(t=t.replace(/^xlink:?/,""));null==o||!1===o?s?e.removeAttributeNS("http://www.w3.org/1999/xlink",t.toLowerCase()):e.removeAttribute(t):"function"!=typeof o&&(s?e.setAttributeNS("http://www.w3.org/1999/xlink",t.toLowerCase(),o):e.setAttribute(t,o))}else e.className=o||""}function _(e){return this._listeners[e.type](e)}var y=[],g=0,b=!1,C=!1;function x(){for(var e;e=y.shift();)e.componentDidMount&&e.componentDidMount()}function N(e,t,n,o,r,i){g++||(b=null!=r&&void 0!==r.ownerSVGElement,C=null!=e&&!("__preactattr_"in e));var l=function e(t,n,o,r,i){var l=t,a=b;if(null!=n&&"boolean"!=typeof n||(n=""),"string"==typeof n||"number"==typeof n)return t&&void 0!==t.splitText&&t.parentNode&&(!t._component||i)?t.nodeValue!=n&&(t.nodeValue=n):(l=document.createTextNode(n),t&&(t.parentNode&&t.parentNode.replaceChild(l,t),w(t,!0))),l.__preactattr_=!0,l;var s,p,u=n.nodeName;if("function"==typeof u)return function(e,t,n,o){for(var r=e&&e._component,i=r,l=e,a=r&&e._componentConstructor===t.nodeName,s=a,p=h(t);r&&!s&&(r=r._parentComponent);)s=r.constructor===t.nodeName;return r&&s&&(!o||r._component)?(U(r,p,3,n,o),e=r.base):(i&&!a&&(T(i),e=l=null),r=P(t.nodeName,p,n),e&&!r.nextBase&&(r.nextBase=e,l=null),U(r,p,1,n,o),e=r.base,l&&e!==l&&(l._component=null,w(l,!1))),e}(t,n,o,r);if(b="svg"===u||"foreignObject"!==u&&b,u=String(u),(!t||!d(t,u))&&(s=u,(p=b?document.createElementNS("http://www.w3.org/2000/svg",s):document.createElement(s)).normalizedNodeName=s,l=p,t)){for(;t.firstChild;)l.appendChild(t.firstChild);t.parentNode&&t.parentNode.replaceChild(l,t),w(t,!0)}var c=l.firstChild,f=l.__preactattr_,_=n.children;if(null==f){f=l.__preactattr_={};for(var y=l.attributes,g=y.length;g--;)f[y[g].name]=y[g].value}return!C&&_&&1===_.length&&"string"==typeof _[0]&&null!=c&&void 0!==c.splitText&&null==c.nextSibling?c.nodeValue!=_[0]&&(c.nodeValue=_[0]):(_&&_.length||null!=c)&&function(t,n,o,r,i){var l,a,s,p,u,c,f,h,m=t.childNodes,_=[],y={},g=0,b=0,C=m.length,x=0,N=n?n.length:0;if(0!==C)for(var k=0;k<C;k++){var S=m[k],P=S.__preactattr_;null!=(B=N&&P?S._component?S._component.__key:P.key:null)?(g++,y[B]=S):(P||(void 0!==S.splitText?!i||S.nodeValue.trim():i))&&(_[x++]=S)}if(0!==N)for(k=0;k<N;k++){var B;if(u=null,null!=(B=(p=n[k]).key))g&&void 0!==y[B]&&(u=y[B],y[B]=void 0,g--);else if(b<x)for(l=b;l<x;l++)if(void 0!==_[l]&&(c=a=_[l],h=i,"string"==typeof(f=p)||"number"==typeof f?void 0!==c.splitText:"string"==typeof f.nodeName?!c._componentConstructor&&d(c,f.nodeName):h||c._componentConstructor===f.nodeName)){u=a,_[l]=void 0,l===x-1&&x--,l===b&&b++;break}u=e(u,p,o,r),s=m[k],u&&u!==t&&u!==s&&(null==s?t.appendChild(u):u===s.nextSibling?v(s):t.insertBefore(u,s))}if(g)for(var k in y)void 0!==y[k]&&w(y[k],!1);for(;b<=x;)void 0!==(u=_[x--])&&w(u,!1)}(l,_,o,r,C||null!=f.dangerouslySetInnerHTML),function(e,t,n){var o;for(o in n)t&&null!=t[o]||null==n[o]||m(e,o,n[o],n[o]=void 0,b);for(o in t)"children"===o||"innerHTML"===o||o in n&&t[o]===("value"===o||"checked"===o?e[o]:n[o])||m(e,o,n[o],n[o]=t[o],b)}(l,n.attributes,f),b=a,l}(e,t,n,o,i);return r&&l.parentNode!==r&&r.appendChild(l),--g||(C=!1,i||x()),l}function w(e,t){var n=e._component;n?T(n):(null!=e.__preactattr_&&a(e.__preactattr_.ref,null),!1!==t&&null!=e.__preactattr_||v(e),k(e))}function k(e){for(e=e.lastChild;e;){var t=e.previousSibling;w(e,!0),e=t}}var S=[];function P(e,t,n){var o,r=S.length;for(e.prototype&&e.prototype.render?(o=new e(t,n),M.call(o,t,n)):((o=new M(t,n)).constructor=e,o.render=B);r--;)if(S[r].constructor===e)return o.nextBase=S[r].nextBase,S.splice(r,1),o;return o}function B(e,t,n){return this.constructor(e,n)}function U(e,t,o,r,i){e._disable||(e._disable=!0,e.__ref=t.ref,e.__key=t.key,delete t.ref,delete t.key,void 0===e.constructor.getDerivedStateFromProps&&(!e.base||i?e.componentWillMount&&e.componentWillMount():e.componentWillReceiveProps&&e.componentWillReceiveProps(t,r)),r&&r!==e.context&&(e.prevContext||(e.prevContext=e.context),e.context=r),e.prevProps||(e.prevProps=e.props),e.props=t,e._disable=!1,0!==o&&(1!==o&&!1===n.syncComponentUpdates&&e.base?c(e):L(e,1,i)),a(e.__ref,e))}function L(e,t,n,o){if(!e._disable){var r,i,a,s=e.props,p=e.state,u=e.context,c=e.prevProps||s,f=e.prevState||p,d=e.prevContext||u,v=e.base,m=e.nextBase,_=v||m,b=e._component,C=!1,k=d;if(e.constructor.getDerivedStateFromProps&&(p=l(l({},p),e.constructor.getDerivedStateFromProps(s,p)),e.state=p),v&&(e.props=c,e.state=f,e.context=d,2!==t&&e.shouldComponentUpdate&&!1===e.shouldComponentUpdate(s,p,u)?C=!0:e.componentWillUpdate&&e.componentWillUpdate(s,p,u),e.props=s,e.state=p,e.context=u),e.prevProps=e.prevState=e.prevContext=e.nextBase=null,e._dirty=!1,!C){r=e.render(s,p,u),e.getChildContext&&(u=l(l({},u),e.getChildContext())),v&&e.getSnapshotBeforeUpdate&&(k=e.getSnapshotBeforeUpdate(c,f));var S,B,M=r&&r.nodeName;if("function"==typeof M){var W=h(r);(i=b)&&i.constructor===M&&W.key==i.__key?U(i,W,1,u,!1):(S=i,e._component=i=P(M,W,u),i.nextBase=i.nextBase||m,i._parentComponent=e,U(i,W,0,u,!1),L(i,1,n,!0)),B=i.base}else a=_,(S=b)&&(a=e._component=null),(_||1===t)&&(a&&(a._component=null),B=N(a,r,u,n||!v,_&&_.parentNode,!0));if(_&&B!==_&&i!==b){var D=_.parentNode;D&&B!==D&&(D.replaceChild(B,_),S||(_._component=null,w(_,!1)))}if(S&&T(S),e.base=B,B&&!o){for(var E=e,V=e;V=V._parentComponent;)(E=V).base=B;B._component=E,B._componentConstructor=E.constructor}}for(!v||n?y.push(e):C||e.componentDidUpdate&&e.componentDidUpdate(c,f,k);e._renderCallbacks.length;)e._renderCallbacks.pop().call(e);g||o||x()}}function T(e){var t=e.base;e._disable=!0,e.componentWillUnmount&&e.componentWillUnmount(),e.base=null;var n=e._component;n?T(n):t&&(null!=t.__preactattr_&&a(t.__preactattr_.ref,null),e.nextBase=t,v(t),S.push(e),k(t)),a(e.__ref,null)}function M(e,t){this._dirty=!0,this.context=t,this.props=e,this.state=this.state||{},this._renderCallbacks=[]}l(M.prototype,{setState:function(e,t){this.prevState||(this.prevState=this.state),this.state=l(l({},this.state),"function"==typeof e?e(this.state,this.props):e),t&&this._renderCallbacks.push(t),c(this)},forceUpdate:function(e){e&&this._renderCallbacks.push(e),L(this,2)},render:function(){}});var W=function(e,t,n,o){for(var r=1;r<t.length;r++){var i=t[r++],l="number"==typeof i?n[i]:i;1===t[r]?o[0]=l:2===t[r]?(o[1]=o[1]||{})[t[++r]]=l:3===t[r]?o[1]=Object.assign(o[1]||{},l):o.push(t[r]?e.apply(null,W(e,l,n,["",null])):l)}return o},D=function(e){for(var t,n,o=1,r="",i="",l=[0],a=function(e){1===o&&(e||(r=r.replace(/^\s*\n\s*|\s*\n\s*$/g,"")))?l.push(e||r,0):3===o&&(e||r)?(l.push(e||r,1),o=2):2===o&&"..."===r&&e?l.push(e,3):2===o&&r&&!e?l.push(!0,2,r):4===o&&n&&(l.push(e||r,2,n),n=""),r=""},s=0;s<e.length;s++){s&&(1===o&&a(),a(s));for(var p=0;p<e[s].length;p++)t=e[s][p],1===o?"<"===t?(a(),l=[l],o=3):r+=t:i?t===i?i="":r+=t:'"'===t||"'"===t?i=t:">"===t?(a(),o=1):o&&("="===t?(o=4,n=r,r=""):"/"===t?(a(),3===o&&(l=l[0]),o=l,(l=l[0]).push(o,4),o=0):" "===t||"\t"===t||"\n"===t||"\r"===t?(a(),o=2):r+=t)}return a(),l},E="function"==typeof Map,V=E?new Map:{},A=E?function(e){var t=V.get(e);return t||V.set(e,t=D(e)),t}:function(e){for(var t="",n=0;n<e.length;n++)t+=e[n].length+"-"+e[n];return V[t]||(V[t]=D(e))},j=function(e){var t=W(this,A(e),arguments,[]);return t.length>1?t:t[0]}.bind(i);e.h=i,e.html=j,e.render=function(e,t){!function(t,n,o){N(o,e,{},!1,n,!1)}(0,t,t.firstElementChild)},e.Component=M});

},{}],"Todo/util.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.uuid = uuid;
exports.pluralize = pluralize;
exports.store = store;

function uuid() {
  var uuid = '';

  for (var i = 0; i < 32; i++) {
    var random = Math.random() * 16 | 0;

    if (i === 8 || i === 12 || i === 16 || i === 20) {
      uuid += '-';
    }

    uuid += (i === 12 ? 4 : i === 16 ? random & 3 | 8 : random).toString(16);
  }

  return uuid;
}

function pluralize(count, word) {
  return count === 1 ? word : word + 's';
}

function store(namespace, data) {
  if (data) return localStorage[namespace] = JSON.stringify(data);
  var store = localStorage[namespace];
  return store && JSON.parse(store) || [];
}
},{}],"Todo/model.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _util = require("./util");

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var TodoModel =
/*#__PURE__*/
function () {
  function TodoModel(key, sub) {
    _classCallCheck(this, TodoModel);

    this.key = key;
    this.todos = (0, _util.store)(key) || [];
    this.onChanges = [sub];
  }

  _createClass(TodoModel, [{
    key: "inform",
    value: function inform() {
      (0, _util.store)(this.key, this.todos);
      this.onChanges.forEach(function (cb) {
        return cb();
      });
    }
  }, {
    key: "addTodo",
    value: function addTodo(title) {
      this.todos = this.todos.concat({
        id: (0, _util.uuid)(),
        title: title,
        completed: false
      });
      this.inform();
    }
  }, {
    key: "toggleAll",
    value: function toggleAll(completed) {
      this.todos = this.todos.map(function (todo) {
        return _objectSpread({}, todo, {
          completed: completed
        });
      });
      this.inform();
    }
  }, {
    key: "toggle",
    value: function toggle(todoToToggle) {
      this.todos = this.todos.map(function (todo) {
        return todo !== todoToToggle ? todo : _objectSpread({}, todo, {
          completed: !todo.completed
        });
      });
      this.inform();
    }
  }, {
    key: "destroy",
    value: function destroy(todo) {
      this.todos = this.todos.filter(function (t) {
        return t !== todo;
      });
      this.inform();
    }
  }, {
    key: "save",
    value: function save(todoToSave, title) {
      this.todos = this.todos.map(function (todo) {
        return todo !== todoToSave ? todo : _objectSpread({}, todo, {
          title: title
        });
      });
      this.inform();
    }
  }, {
    key: "clearCompleted",
    value: function clearCompleted() {
      this.todos = this.todos.filter(function (todo) {
        return !todo.completed;
      });
      this.inform();
    }
  }]);

  return TodoModel;
}();

exports.default = TodoModel;
},{"./util":"Todo/util.js"}],"Todo/footer.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _standalone = require("htm/preact/standalone.js");

var _util = require("./util");

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n            <button class=\"clear-completed\" onClick=\"{onClearCompleted}\">\n              Clear completed\n            </button>\n          "]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n      <footer class=\"footer\">\n        <span class=\"todo-count\">\n          <strong>", "</strong> ", " left\n        </span>\n        <ul class=\"filters\">\n          <li>\n            <a href=\"#/\" class=", ">All</a>\n          </li>\n          ", "\n          <li>\n            <a href=\"#/active\" class=", "\n              >Active</a\n            >\n          </li>\n          ", "\n          <li>\n            <a\n              href=\"#/completed\"\n              class=", "\n              >Completed</a\n            >\n          </li>\n        </ul>\n        ", "\n      </footer>\n    "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var TodoFooter =
/*#__PURE__*/
function (_Component) {
  _inherits(TodoFooter, _Component);

  function TodoFooter() {
    _classCallCheck(this, TodoFooter);

    return _possibleConstructorReturn(this, _getPrototypeOf(TodoFooter).apply(this, arguments));
  }

  _createClass(TodoFooter, [{
    key: "render",
    value: function render(_ref) {
      var nowShowing = _ref.nowShowing,
          count = _ref.count,
          completedCount = _ref.completedCount,
          onClearCompleted = _ref.onClearCompleted;
      return (0, _standalone.html)(_templateObject(), count, (0, _util.pluralize)(count, "item"), nowShowing == "all" && "selected", " ", nowShowing == "active" && "selected", " ", nowShowing == "completed" && "selected", completedCount > 0 && (0, _standalone.html)(_templateObject2()));
    }
  }]);

  return TodoFooter;
}(_standalone.Component);

exports.default = TodoFooter;
},{"htm/preact/standalone.js":"../node_modules/htm/preact/standalone.js","./util":"Todo/util.js"}],"Todo/item.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _standalone = require("htm/preact/standalone.js");

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n            <input\n              class=\"edit\"\n              value=", "\n              onBlur=", "\n              onInput=", "\n              onKeyDown=", "\n            />\n          "]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n      <li class=", ">\n        <div class=\"view\">\n          <input\n            class=\"toggle\"\n            type=\"checkbox\"\n            checked=", "\n            onChange=", "\n          />\n          <label onDblClick=", ">", "</label>\n          <button class=\"destroy\" onClick=", " />\n        </div>\n        ", "\n      </li>\n    "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var ESCAPE_KEY = 27;
var ENTER_KEY = 13;

var TodoItem =
/*#__PURE__*/
function (_Component) {
  _inherits(TodoItem, _Component);

  function TodoItem() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, TodoItem);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(TodoItem)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "handleSubmit", function () {
      var _this$props = _this.props,
          onSave = _this$props.onSave,
          onDestroy = _this$props.onDestroy,
          todo = _this$props.todo,
          val = _this.state.editText.trim();

      if (val) {
        onSave(todo, val);

        _this.setState({
          editText: val
        });
      } else {
        onDestroy(todo);
      }
    });

    _defineProperty(_assertThisInitialized(_this), "handleEdit", function () {
      var _this$props2 = _this.props,
          onEdit = _this$props2.onEdit,
          todo = _this$props2.todo;
      onEdit(todo);

      _this.setState({
        editText: todo.title
      });
    });

    _defineProperty(_assertThisInitialized(_this), "toggle", function (e) {
      var _this$props3 = _this.props,
          onToggle = _this$props3.onToggle,
          todo = _this$props3.todo;
      onToggle(todo);
      e.preventDefault();
    });

    _defineProperty(_assertThisInitialized(_this), "handleTextInput", function (e) {
      _this.setState({
        editText: e.target.value
      });
    });

    _defineProperty(_assertThisInitialized(_this), "handleKeyDown", function (e) {
      if (e.which === ESCAPE_KEY) {
        var todo = _this.props.todo;

        _this.setState({
          editText: todo.title
        });

        _this.props.onCancel(todo);
      } else if (e.which === ENTER_KEY) {
        _this.handleSubmit();
      }
    });

    _defineProperty(_assertThisInitialized(_this), "handleDestroy", function () {
      _this.props.onDestroy(_this.props.todo);
    });

    return _this;
  }

  _createClass(TodoItem, [{
    key: "componentDidUpdate",
    // shouldComponentUpdate({ todo, editing, editText }) {
    // 	return (
    // 		todo !== this.props.todo ||
    // 		editing !== this.props.editing ||
    // 		editText !== this.state.editText
    // 	);
    // }
    value: function componentDidUpdate() {
      var node = this.base && this.base.querySelector(".edit");
      if (node) node.focus();
    }
  }, {
    key: "render",
    value: function render(_ref, _ref2) {
      var _ref$todo = _ref.todo,
          title = _ref$todo.title,
          completed = _ref$todo.completed,
          onToggle = _ref.onToggle,
          onDestroy = _ref.onDestroy,
          editing = _ref.editing;
      var editText = _ref2.editText;
      return (0, _standalone.html)(_templateObject(), (completed, editing), completed, this.toggle, this.handleEdit, title, this.handleDestroy, editing && (0, _standalone.html)(_templateObject2(), editText, this.handleSubmit, this.handleTextInput, this.handleKeyDown));
    }
  }]);

  return TodoItem;
}(_standalone.Component);

exports.default = TodoItem;
},{"htm/preact/standalone.js":"../node_modules/htm/preact/standalone.js"}],"Todo/index.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _standalone = require("htm/preact/standalone.js");

var _model = _interopRequireDefault(require("./model"));

var _footer = _interopRequireDefault(require("./footer"));

var _item = _interopRequireDefault(require("./item"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _templateObject4() {
  var data = _taggedTemplateLiteral(["\n              <", "\n                count=", "\n                completedCount=", "\n                nowShowing=", "\n                onClearCompleted=", "\n              />\n            "]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n                      <", "\n                        todo=", "\n                        onToggle=", "\n                        onDestroy=", "\n                        onEdit=", "\n                        editing=", "\n                        onSave=", "\n                        onCancel=", "\n                      />\n                    "]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n              <section class=\"main\">\n                <input\n                  class=\"toggle-all\"\n                  type=\"checkbox\"\n                  onChange=", "\n                  checked=", "\n                />\n                <ul class=\"todolist\">\n                  ", "\n                </ul>\n              </section>\n            "]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n      <div>\n        <header class=\"header\">\n          <input\n            class=\"new-todo\"\n            placeholder=\"What needs to be done?\"\n            value=", "\n            onKeyDown=", "\n            onInput=", "\n            autofocus=", "\n          />\n        </header>\n\n        ", "\n        ", "\n      </div>\n    "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _objectDestructuringEmpty(obj) { if (obj == null) throw new TypeError("Cannot destructure undefined"); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var ENTER_KEY = 13;
var FILTERS = {
  all: function all(todo) {
    return true;
  },
  active: function active(todo) {
    return !todo.completed;
  },
  completed: function completed(todo) {
    return todo.completed;
  }
};

var Todo =
/*#__PURE__*/
function (_Component) {
  _inherits(Todo, _Component);

  function Todo() {
    var _this;

    _classCallCheck(this, Todo);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Todo).call(this));

    _defineProperty(_assertThisInitialized(_this), "handleNewTodoKeyDown", function (e) {
      if (e.keyCode !== ENTER_KEY) return;
      e.preventDefault();

      var val = _this.state.newTodo.trim();

      if (val) {
        _this.model.addTodo(val);

        _this.setState({
          newTodo: ""
        });
      }
    });

    _defineProperty(_assertThisInitialized(_this), "handleNewTodoInput", function (e) {
      _this.setState({
        newTodo: e.target.value
      });
    });

    _defineProperty(_assertThisInitialized(_this), "toggleAll", function (event) {
      var checked = event.target.checked;

      _this.model.toggleAll(checked);
    });

    _defineProperty(_assertThisInitialized(_this), "toggle", function (todo) {
      _this.model.toggle(todo);
    });

    _defineProperty(_assertThisInitialized(_this), "destroy", function (todo) {
      _this.model.destroy(todo);
    });

    _defineProperty(_assertThisInitialized(_this), "edit", function (todo) {
      _this.setState({
        editing: todo.id
      });
    });

    _defineProperty(_assertThisInitialized(_this), "save", function (todoToSave, text) {
      _this.model.save(todoToSave, text);

      _this.setState({
        editing: null
      });
    });

    _defineProperty(_assertThisInitialized(_this), "cancel", function () {
      _this.setState({
        editing: null
      });
    });

    _defineProperty(_assertThisInitialized(_this), "clearCompleted", function () {
      _this.model.clearCompleted();
    });

    _this.model = new _model.default("preact-todos", function () {
      return _this.setState({});
    });
    addEventListener("hashchange", _this.handleRoute.bind(_assertThisInitialized(_this)));

    _this.handleRoute();

    return _this;
  }

  _createClass(Todo, [{
    key: "handleRoute",
    value: function handleRoute() {
      var nowShowing = String(location.hash || "").split("/").pop();

      if (!FILTERS[nowShowing]) {
        nowShowing = "all";
      }

      this.setState({
        nowShowing: nowShowing
      });
    }
  }, {
    key: "render",
    value: function render(_ref, _ref2) {
      var _this2 = this;

      _objectDestructuringEmpty(_ref);

      var _ref2$nowShowing = _ref2.nowShowing,
          nowShowing = _ref2$nowShowing === void 0 ? ALL_TODOS : _ref2$nowShowing,
          newTodo = _ref2.newTodo,
          editing = _ref2.editing;
      var todos = this.model.todos,
          shownTodos = todos.filter(FILTERS[nowShowing]),
          activeTodoCount = todos.reduce(function (a, todo) {
        return a + (todo.completed ? 0 : 1);
      }, 0),
          completedCount = todos.length - activeTodoCount;
      return (0, _standalone.html)(_templateObject(), newTodo, this.handleNewTodoKeyDown, this.handleNewTodoInput, true, todos.length ? (0, _standalone.html)(_templateObject2(), this.toggleAll, activeTodoCount === 0, shownTodos.map(function (todo) {
        return (0, _standalone.html)(_templateObject3(), _item.default, todo, _this2.toggle, _this2.destroy, _this2.edit, editing === todo.id, _this2.save, _this2.cancel);
      })) : null, activeTodoCount || completedCount ? (0, _standalone.html)(_templateObject4(), _footer.default, activeTodoCount, completedCount, nowShowing, this.clearCompleted) : null);
    }
  }]);

  return Todo;
}(_standalone.Component);

exports.default = Todo;
},{"htm/preact/standalone.js":"../node_modules/htm/preact/standalone.js","./model":"Todo/model.js","./footer":"Todo/footer.js","./item":"Todo/item.js"}],"index.js":[function(require,module,exports) {
"use strict";

var _standalone = require("htm/preact/standalone.js");

var _Todo = _interopRequireDefault(require("./Todo"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n    <nav class=\"navbar is-fixed-top is-transparent\" role=\"navigation\">\n      header\n    </nav>\n  "]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n      <div class=\"app\">\n        ", "\n\n        <", " />\n\n        <pre\n          style=", "\n        >\n          ", "\n        </pre\n        >\n      </div>\n    "]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n      <", " />\n    "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var start = function start() {
  console.log("starting...");
  (0, _standalone.render)((0, _standalone.html)(_templateObject(), App), document.body);
}; // ==========


var App =
/*#__PURE__*/
function (_Component) {
  _inherits(App, _Component);

  function App() {
    var _this;

    _classCallCheck(this, App);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(App).call(this));
    _this.state = {
      test: {
        color: "#222"
      }
    };
    return _this;
  }

  _createClass(App, [{
    key: "render",
    value: function render(props, state) {
      return (0, _standalone.html)(_templateObject2(), Header(), _Todo.default, ["background:#fffd", "color:#333", "position:fixed", "bottom:0", "width:100%", "font-size:x-small"].join(";"), JSON.stringify(state, 0, 2));
    }
  }]);

  return App;
}(_standalone.Component);

var Header = function Header() {
  return (0, _standalone.html)(_templateObject3());
};

document.addEventListener("DOMContentLoaded", start, false);
},{"htm/preact/standalone.js":"../node_modules/htm/preact/standalone.js","./Todo":"Todo/index.js"}],"../node_modules/parcel-bundler/src/builtins/hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };
  module.bundle.hotData = null;
}

module.bundle.Module = Module;
var parent = module.bundle.parent;

if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = "" || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + "55446" + '/');

  ws.onmessage = function (event) {
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      console.clear();
      data.assets.forEach(function (asset) {
        hmrApply(global.parcelRequire, asset);
      });
      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          hmrAccept(global.parcelRequire, asset.id);
        }
      });
    }

    if (data.type === 'reload') {
      ws.close();

      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');
      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);
      removeErrorOverlay();
      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);

  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID; // html encode message and stack trace

  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;
  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';
  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];

      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAccept(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAccept(bundle.parent, id);
  }

  var cached = bundle.cache[id];
  bundle.hotData = {};

  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);
  cached = bundle.cache[id];

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });

    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAccept(global.parcelRequire, id);
  });
}
},{}]},{},["../node_modules/parcel-bundler/src/builtins/hmr-runtime.js","index.js"], null)
//# sourceMappingURL=/src.e31bb0bc.map